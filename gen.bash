#!/bin/bash
source $1
FILE=$(realpath $0)

cat << EOF > ~/.bashrc
#:$FILE:$LINENO
[[ \$- != *i* ]] && return
$( echo "$aliases" | sed 's/^/alias /g')
tput cvvis || export TERM=xterm-256color
prompt_command()
{
    local status=\$?
    [ \$status != 0 ] &&
        echo "\$(tput setaf $COLOR_ERROR)ERROR CODE: \$status"
    PS1="\[\$(tput setaf $COLOR_HOST)\]"' \u@\h:'
    PS1+="\[\$(tput setaf $COLOR_PROMPT)\]\w\n"
    PS1+="\[\$(tput setaf $COLOR_PROMPT)\]"' \$ '"\[\$(tput sgr0)\]"
}
PROMPT_COMMAND=prompt_command
EOF

cat << EOF > ~/.bash_profile
#:$FILE:$LINENO
. ~/.bashrc
export PATH="\$PATH:$HOME/.local/bin"
$( echo "$exports" | sed 's/^/export /g')
export DOTFILES=~/.config/dotfiles
[[ \$(tty) == /dev/tty1 ]] && $( echo "$runit" | sed 's/$/ ||/g') true
EOF
